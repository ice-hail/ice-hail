/**
* @copyright 2019 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

const BoxShape = require("../../src/classes/hkpBoxShape");
const BinaryFile = require("../../src/binaryFile");

describe(BoxShape.name, () => 
{
    test('pointer', () => {
        const file = new BinaryFile();
        const entry = new BoxShape(file, {
            sizeHalf: [1.0, 2.0, 3.0]
        });
        entry.create();

        expect(entry.pointer.length).toEqual(0);
    });

    test('integration test', () => {
        const file = new BinaryFile();
        const entry = new BoxShape(file, {
            sizeHalf: [1.0, 2.0, 3.0]
        });
        entry.create();

        expect(file.getBuffer()).toEqualBinary(
`00000000: 0000 0000 0000 0000 0004 0000 1000 8002  ................
00000010: 3d4c cccd 0000 0000 0000 0000 0000 0000  =LÌÍ............
00000020: 3f80 0000 4000 0000 4040 0000 0000 0000  ?...@...@@......`
        );
    });
});