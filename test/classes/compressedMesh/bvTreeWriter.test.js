/**
* @copyright 2019 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

const {writeBvTree} = require('../../../src/classes/compressedMesh/bvTreeWriter');
const BvTree = require('../../../src/classes/bvTree/tree');
const {Domain, DomainEntry} = require('../../../src/math/domain');
const {readBvTree} = require('../../../src/classes/compressedMesh/bvTreeReader');
const BinaryFile = require('../../../src/binaryFile');

describe("compressedMesh", () => 
{
    describe("bvTree.writeBvTree", () => 
    {
        test('tree with single entry', () => {
            const testFile = new BinaryFile();
            const tree = new DomainEntry(new Domain([-1,-1,-1], [1,1,1]), 2);

            writeBvTree(testFile, tree);
            expect(testFile.getBuffer()).toEqual(Buffer.from([
                0x00, 0x00, 0x00, 4
            ]));
        });

        test('nested tree, 3 level', () => {
            const testFile = new BinaryFile();
            const tree = new BvTree(
                new Domain([-1,-1,-1], [1,1,1]),
                new BvTree(
                    new Domain([-1,-1,-1], [1,1,1]),
                    new DomainEntry(new Domain([-1,-1,-1], [1,1,1]), 1),
                    new DomainEntry(new Domain([-1,-1,-1], [1,1,1]), 3),
                ),
                new DomainEntry(new Domain([-1,-1,-1], [1,1,1]), 2),
            );

            writeBvTree(testFile, tree);
            expect(testFile.getBuffer()).toEqual(Buffer.from([
                0x00, 0x00, 0x00, 5,
                0x00, 0x00, 0x00, 3, // child-0
                0x00, 0x00, 0x00, 2, // child-0-0
                0x00, 0x00, 0x00, 6, // child-0-1
                0x00, 0x00, 0x00, 4, // child-1
            ]));
        });

        test('nested tree, multi level correct order', () => {
            const testFile = new BinaryFile();
            const {tree} = require('./testFiles/multiLevel.json');

            writeBvTree(testFile, tree);
            expect(testFile.getBuffer()).toEqual(Buffer.from([
                0x00, 0x00, 0x00, 0x03, 
                0x00, 0x00, 0x00, 0x0A, 
                0x00, 0x00, 0x00, 0x11, 
                0x00, 0x00, 0x00, 0x0D,
                0x00, 0x00, 0x00, 0x09, 
                0x00, 0x00, 0x00, 0x05, 
                0x00, 0x00, 0x00, 0x03, 
                0x00, 0x00, 0x00, 0x08,
                0x00, 0x00, 0x00, 0x12, 
                0x00, 0x00, 0x00, 0x03, 
                0x00, 0x00, 0x00, 0x10, 
                0x00, 0x00, 0x00, 0x02,
                0x00, 0x00, 0x00, 0x03, 
                0x00, 0x00, 0x00, 0x00, 
                0x00, 0x00, 0x00, 0x14, 
                0x00, 0x00, 0x00, 0x03,
                0x00, 0x00, 0x00, 0x0E, 
                0x00, 0x00, 0x00, 0x06, 
                0x00, 0x00, 0x00, 0x03, 
                0x00, 0x00, 0x00, 0x04,
                0x00, 0x00, 0x00, 0x0C,            
            ]));
        });

        test('nested multi-level tree, write->read test', () => {
            const testFile = new BinaryFile();
            const {tree} = require('./testFiles/multiLevel.json');

            writeBvTree(testFile, tree);

            const testDomain = {min: [0,0,0], max: [1,1,1]};
            testFile.pos(0);
            const readTree = readBvTree(testFile, 21, testDomain);

            expect(tree).toEqual(readTree);
        });
    });
});  