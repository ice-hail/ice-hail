/**
* @copyright 2019 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

const ConvexVerticesShape = require("../../src/classes/hkpConvexVerticesShape");
const BinaryFile = require("../../src/binaryFile");

const vertexData = {
    vertices: [
        [1.0, 2.0, 3.0],
        [4.0, 4.0, 4.0],
        [9.0, 8.0, 7.0],
        [0.0, -1.0, -1.0],
    ],
    indices: [
        [0, 1, 2, 3]
    ]
};

describe(ConvexVerticesShape.name, () => 
{
    test('pointer', () => {
        const file = new BinaryFile();
        const entry = new ConvexVerticesShape(file, vertexData);
        entry._create();

        expect(entry.pointer).toEqual([
            0x40, 0x70, 0x54, 0xA0
        ]);
    });

    test('integration test', () => {
        const file = new BinaryFile();
        const entry = new ConvexVerticesShape(file, vertexData);
        entry._create();

        expect(file.getBuffer()).toEqualBinary(
`00000000: 0000 0000 0000 0000 0004 0000 1000 8002  ................
00000010: 3d4c cccd 0000 0000 0000 0000 0000 0000  =LÌÍ............
00000020: 4097 3333 4097 3333 4086 6666 0000 0000  @.33@.33@.ff....
00000030: 4090 0000 4060 0000 4040 0000 0000 0000  @...@\`..@@......
00000040: 0000 0000 0000 0001 8000 0001 0000 0004  ................
00000050: 0000 0000 0000 0000 0000 0001 8000 0001  ................
00000060: 0000 0000 0000 0000 0000 0000 0000 0000  ................
00000070: 3f80 0000 4080 0000 4110 0000 0000 0000  ?...@...A.......
00000080: 4000 0000 4080 0000 4100 0000 bf80 0000  @...@...A...¿...
00000090: 4040 0000 4080 0000 40e0 0000 bf80 0000  @@..@...@à..¿...
000000a0: 3ed1 05ec bf51 05ec 3ed1 05ec 0000 0000  >Ñ.ì¿Q.ì>Ñ.ì....`
        );
    });

    describe('groupVertices', () => 
    {
        test('empty', () => 
        {   
            const file = new BinaryFile();
            const mesh = new ConvexVerticesShape(file, vertexData);
            expect(mesh._groupVertices([])).toEqual([]);
            expect(mesh._groupVertices(null)).toEqual([]);
            expect(mesh._groupVertices()).toEqual([]);
        });

        test('not 3 elements per array', () => 
        {   
            const file = new BinaryFile();
            const mesh = new ConvexVerticesShape(file, vertexData);
            expect(() => mesh._groupVertices([
                [1,2,3],
                [1],
                [1,2,3]
            ])).toThrow();
        });

        test('4 vectors should be grouped into a matrix', () => 
        {   
            const file = new BinaryFile();
            const mesh = new ConvexVerticesShape(file, vertexData);
            expect(mesh._groupVertices([
                [1.0, 2.0, 3.0],
                [4.0, 5.0, 6.0],
                [7.0, 8.0, 9.0],
                [0.0, 1.0, 2.0],
            ])).toEqual([
                [
                    [1.0, 2.0, 3.0],
                    [4.0, 5.0, 6.0],
                    [7.0, 8.0, 9.0],
                    [0.0, 1.0, 2.0],
                ]
            ]);
        });

        test('single vector should be padded to 4', () => 
        {   
            const file = new BinaryFile();
            const mesh = new ConvexVerticesShape(file, vertexData);
            expect(mesh._groupVertices([
                [5.0, 6.0, 6.0]
            ])).toEqual([
                [
                    [5.0, 6.0, 6.0],
                    [5.0, 6.0, 6.0],
                    [5.0, 6.0, 6.0],
                    [5.0, 6.0, 6.0],
                ]
            ]);
        });

        test('6 vectors should be padded to 12', () => 
        {   
            const file = new BinaryFile();
            const mesh = new ConvexVerticesShape(file, vertexData);
            expect(mesh._groupVertices([
                [1.0, 2.0, 3.0],
                [4.0, 5.0, 6.0],
                [7.0, 8.0, 9.0],
                [0.0, 1.0, 2.0],
                [2.0, 3.0, 5.0],
                [6.0, 7.0, 8.0],
            ])).toEqual([
                [
                    [1.0, 2.0, 3.0],
                    [4.0, 5.0, 6.0],
                    [7.0, 8.0, 9.0],
                    [0.0, 1.0, 2.0],
                ],
                [
                    [2.0, 3.0, 5.0],
                    [6.0, 7.0, 8.0],
                    [6.0, 7.0, 8.0],
                    [6.0, 7.0, 8.0],
                ]
            ]);
        });
    
    });
});