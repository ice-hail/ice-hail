/**
* @copyright 2019 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

const ListShape = require("../../src/classes/hkpListShape");
const BinaryFile = require("../../src/binaryFile");

describe(ListShape.name, () => 
{
    test('pointer', () => {
        const file = new BinaryFile();
        const entry = new ListShape(file, {
            children: [],
            sizeHalf: [1.0, 2.0, 3.0]
        });
        entry._create();

        expect(entry.pointer).toEqual([
            0x18, 0x70
        ]);
    });

    test('integration test', () => {
        const file = new BinaryFile();
        const entry = new ListShape(file, {
            name: "Box_test",
            children: [{
                radius: 1.0
            }, {
                radius: 1.0
            }]
        });
        entry._create();

        expect(file.getBuffer()).toEqualBinary(
`00000000: 0000 0000 0000 0000 0004 0000 0000 0000  ................
00000010: 0000 0000 0000 0000 0000 0000 0000 0002  ................
00000020: 8000 0002 0000 0000 0000 0000 0000 0000  ................
00000030: 3f86 6666 3f86 6666 3f86 6666 3f80 0000  ?.ff?.ff?.ff?...
00000040: 0000 0000 0000 0000 0000 0000 3d4c cccd  ............=LÌÍ
00000050: ffff ffff ffff ffff ffff ffff ffff ffff  ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ
00000060: ffff ffff ffff ffff ffff ffff ffff ffff  ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ
00000070: 0000 0000 3c00 0008 0000 0000 0000 0000  ....<...........
00000080: 0000 0000 3c00 0008 0000 0000 0000 0000  ....<...........`
        );
    });
});