/**
* @copyright 2019 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

const {getSubDomain} = require('../../../src/classes/bvTree/reader');

describe("bvTree.reader", () => 
{
    describe("getSubDomain", () => 
    {
        test('simple box, data axis test', () => {
            const testDomain = {
                min: [-1.0, -1.0, -1.0],
                max: [ 1.0,  1.0,  1.0]
            };

            expect(getSubDomain([0xFF, 0x00, 0x00], testDomain))
                .toMatchCloseTo({ 
                    min: [ 0.99, -1.0, -1.0], 
                    max: [-0.99,  1.0,   1.0] 
                }, 2);
            expect(getSubDomain([0x00, 0xFF, 0x00], testDomain))
            .toMatchCloseTo({ 
                min: [-1.0,  0.99, -1.0], 
                max: [ 1.0, -0.99,  1.0] 
            }, 2);
            expect(getSubDomain([0x00, 0x00, 0xFF], testDomain))
            .toMatchCloseTo({ 
                min: [-1.0, -1.0,  0.99], 
                max: [ 1.0,  1.0, -0.99] 
            }, 2);
        });

        test('simple box, domain axis test', () => {
            const testData = [0x00, 0x00, 0x00];

            expect(getSubDomain(testData, {
                min: [ -1.0, -2.0, -3.0], 
                max: [  2.0,  4.0,  6.0] 
            }))
            .toMatchCloseTo({ 
                min: [ -1.0, -2.0, -3.0], 
                max: [  2.0,  4.0,  6.0] 
            }, 2);
        });

        test('example box with domain', () => {
            expect(getSubDomain([0xAB, 0x12, 0x0DE], {
                min: [ -1.0, -6.0, 0.0], 
                max: [  2.5,  8.0,  6.25] 
            }))
            .toMatchCloseTo({ 
                min: [0.548, -5.938, 4.673],
                max: [0.626, 7.752, 0.829], 
            }, 2);
        });
    });
});  
