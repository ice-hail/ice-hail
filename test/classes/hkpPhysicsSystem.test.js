/**
* @copyright 2019 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

const PhysicsSystem = require("../../src/classes/hkpPhysicsSystem");
const BinaryFile = require("../../src/binaryFile");

describe(PhysicsSystem.name, () => 
{
    test('pointer, normal', () => {
        const file = new BinaryFile();
        const entry = new PhysicsSystem(file, {
            children: [{}]
        });
        entry._create();

        expect(entry.pointer).toEqual([
            0x08, 0x50, 0x38, 0x60
        ]);
    });

    test('pointer, normal, multiple children', () => {
        const file = new BinaryFile();
        const entry = new PhysicsSystem(file, {
            children: [{}, {}]
        });
        entry._create();

        expect(entry.pointer).toEqual([
            0x08, 0x50, 0x38, 0x60
        ]);
    });

    test('pointer, compound, no children', () => {
        const file = new BinaryFile();
        const entry = new PhysicsSystem(file, {
            compound: {},
            children: []
        });
        entry._create();

        expect(entry.pointer).toEqual([
            0x38, 0x50
        ]);
    });

    test('integration test, with children', () => {
        const file = new BinaryFile();
        const entry = new PhysicsSystem(file, {
            children: [{}]
        });
        entry._create();

        expect(file.getBuffer()).toEqualBinary(
`00000000: 0000 0000 0000 0000 0000 0000 0000 0001  ................
00000010: 8000 0001 0000 0000 0000 0000 8000 0000  ................
00000020: 0000 0000 0000 0000 8000 0000 0000 0000  ................
00000030: 0000 0000 8000 0000 0000 0000 0000 0000  ................
00000040: 0100 0000 0000 0000 0000 0000 0000 0000  ................
00000050: 0000 0000 0000 0000 0000 0000 0000 0000  ................
00000060: 4465 6661 756c 7420 5068 7973 6963 7320  Default Physics 
00000070: 5379 7374 656d 0000 0000 0000 0000 0000  System..........`
        );
    });
});