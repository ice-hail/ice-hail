/**
* @copyright 2019 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

module.exports = [
    {
        name: "RigidBody List",
        extension: ".hkrb",
        read: true,
        write: true,
        files: {
            "model-single-box": 'Simple Box, 1 chunk, 1 box',
            "model-box-8": 'Simple Box, 1 chunk, 8 boxes',
            "model-box-long-name": 'Simple Box, long chunk name',
            "model-box-transform": 'Simple Box, transform with rotation',
            //"model-box-climb": 'Simple Box, flags, climbable',
            "sphere-simple": 'Simple Sphere, translate',
            "sphere-complex": 'Simple Sphere, transform',
            "mesh-simple": 'Simple Mesh',
        }
    },
    {
        name: "Compound File",
        extension: ".hksc",
        read: false, // @TODO read compound info (issue-1)
        write: true,
        files: {
            "compound-empty": "Empty File",
            "multi-obj-norm": "Multiple compressed meshes",
            "multi-obj-material": "Multiple Materials",
        }
    }
];