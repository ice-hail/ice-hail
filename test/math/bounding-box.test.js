/**
* @copyright 2019 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

const BoundingBox = require("../../src/math/bounding-box");
const Vector3 = require("../../src/math/vector3");
BoundingBox.getScalar = () => 1.0;

describe('BoundingBox', () => 
{
    describe('merge', () => 
    {
        test('no box set should return copy of itself', () => 
        {   
            const box = new BoundingBox(
                new Vector3([1.0, 2.0, 3.0]),
                new Vector3([2.0, 3.0, 4.0]),
            );
            const result = box.merge(null).getAsObject();

            expect(result.center).toEqual(  [1.0, 2.0, 3.0]);
            expect(result.sizeHalf).toEqual([2.0, 3.0, 4.0]);
        });

        test('two simple boxes', () => 
        {   
            const box = new BoundingBox(
                new Vector3([-2.0, 2.0, 6.0]),
                new Vector3([2.0, 3.0, 4.0]),
            );
            const result = box.merge(new BoundingBox(
                new Vector3([5.0, -2.0, 5.0]),
                new Vector3([10.0, 1.0, 4.0]),
            )).getAsObject();

            expect(result.center).toEqual(  [5.0, 1.0, 5.5]);
            expect(result.sizeHalf).toEqual([10.0, 4.0, 4.5]);
        });
    });

    describe('getter', () => 
    {
        test('get as min max', () => 
        {   
            const box = new BoundingBox(
                new Vector3([1.0, 0.0, 4.0]),
                new Vector3([2.0, 3.0, 4.0]),
            );
            const result = box.getMinMax();

            expect(result.min.vec).toEqual([-1.0, -3.0, 0.0]);
            expect(result.max.vec).toEqual([3.0, 3.0, 8.0]);
        });
    });

    describe('fromEntry', () => 
    {
        test('empty data', () => 
        {
            const result = BoundingBox.fromEntry({}).getAsObject();
            expect(result.center).toEqual(  [0.0, 0.0, 0.0]);
            expect(result.sizeHalf).toEqual([0.0, 0.0, 0.0]);
        });

        test('simple box', () => 
        {
            const result = BoundingBox.fromEntry({
                sizeHalf: [1.0, 2.0, 3.0]
            }).getAsObject();

            expect(result.center).toEqual(  [0.0, 0.0, 0.0]);
            expect(result.sizeHalf).toEqual([1.0, 2.0, 3.0]);
        });

        test('simple radius/sphere', () => 
        {
            const result = BoundingBox.fromEntry({
                radius: 5.0
            }).getAsObject();

            expect(result.center).toEqual(  [0.0, 0.0, 0.0]);
            expect(result.sizeHalf).toEqual([5.0, 5.0, 5.0]);
        });

        test('box with translation', () => 
        {
            const result = BoundingBox.fromEntry({
                sizeHalf: [1.0, 2.0, 3.0],
                translate: [1.0, -1.0, 2.0]
            }).getAsObject();

            expect(result.center).toEqual(  [1.0, -1.0, 2.0]);
            expect(result.sizeHalf).toEqual([1.0, 2.0, 3.0]);
        });

        test('box with scale', () => 
        {
            const result = BoundingBox.fromEntry({
                sizeHalf: [1.0, 2.0, 3.0],
                scale: [2.0, 1.0, 5.0],
            }).getAsObject();

            expect(result.center).toEqual(  [0.0, 0.0, 0.0]);
            expect(result.sizeHalf).toEqual([2.0, 2.0, 15.0]);
        });
    });

    describe('fromPolygons', () => 
    {
        test('no data should result in zeros', () => 
        {  
            const box = BoundingBox.fromVertices([]).getAsObject();

            expect(box.center).toEqual([0,0,0]);
            expect(box.sizeHalf).toEqual([0,0,0]);
        });

        test('simple single polygon', () => 
        {   
            const box = BoundingBox.fromVertices([
                [0,   0, -2],
                [0.5, 2,  3],
                [1,   4,  4],
                [-1,  0,  0],
            ]).getAsObject();

            expect(box.center).toEqual([0.0, 2, 1]);
            expect(box.sizeHalf).toEqual([1, 2, 3]);
        });

        test('multiple polygons', () => 
        {   
            const box = BoundingBox.fromVertices([
                [0,0,0],
                [1,2,3],
                [0,0,0],
                [0,0,0],

                [0,0,0],
                [-1, -4, -6],
                [0,0,0],
                [0,0,0],
            ]).getAsObject();

            expect(box.center).toEqual([0.0, -1.0, -1.5]);
            expect(box.sizeHalf).toEqual([1, 3, 4.5]);
        });
    });

    describe('fromTree', () => 
    {
        test('simple box, no transform', () => 
        {   
            const box = BoundingBox.fromTree(
            {
                type: "hkpBoxShape",
                sizeHalf: [1.0, 2.0, 3.0]
            }).getAsObject();

            expect(box.center).toEqual([0.0, 0.0, 0.0]);
            expect(box.sizeHalf).toEqual([1.0, 2.0, 3.0]);
        });

        test('simple box, with transform', () => 
        {   
            const box = BoundingBox.fromTree(
            {
                type: "hkpConvexTranslateShape",
                translate: [1.0, -2.0, 0.5],
                children: [
                    {
                        type: "hkpBoxShape",
                        sizeHalf: [1.0, 2.0, 3.0]
                    }
                ]
            }).getAsObject();

            expect(box.center).toEqual([1.0, -2.0, 0.5]);
            expect(box.sizeHalf).toEqual([1.0, 2.0, 3.0]);
        });

        test('simple box, with nested transform', () => 
        {   
            const box = BoundingBox.fromTree(
            {
                type: "hkpConvexTranslateShape",
                translate: [2.0, 1.0, 5.0],
                children: [{
                    type: "hkpConvexTranslateShape",
                    translate: [1.0, -2.0, 0.5],
                    children: [
                        {
                            type: "hkpBoxShape",
                            sizeHalf: [1.0, 2.0, 3.0]
                        }
                    ]
                }]
            }).getAsObject();

            expect(box.center).toEqual([3.0, -1.0, 5.5]);
            expect(box.sizeHalf).toEqual([1.0, 2.0, 3.0]);
        });

        test('multiple boxes, with transform', () => 
        {   
            const box = BoundingBox.fromTree(
            {
                type: "hkpConvexTranslateShape",
                translate: [1.0, -2.0, 0.5],
                children: [
                    {
                        type: "hkpBoxShape",
                        sizeHalf: [1.0, 2.0, 3.0]
                    },
                    {
                        type: "hkpBoxShape",
                        sizeHalf: [2.0, 1.0, 5.0]
                    }

                ]
            }).getAsObject();

            expect(box.center).toEqual([1.0, -2.0, 0.5]);
            expect(box.sizeHalf).toEqual([2.0, 2.0, 5.0]);
        });

        test('container, multiple transforms, one box each', () => 
        {   
            const box = BoundingBox.fromTree({
            type: "someContainerShape",
            children: [
                {
                    type: "hkpConvexTranslateShape",
                    translate: [1.0, -2.0, 0.0],
                    children: [
                        {
                            type: "hkpBoxShape",
                            sizeHalf: [1.0, 1.0, 1.0]
                        }
                    ],
                },{
                    type: "hkpConvexTranslateShape",
                    translate: [4.0, 2.0, -3],
                    children: [
                        {
                            type: "hkpBoxShape",
                            sizeHalf: [2.0, 2.0, 2.0]
                        }
                    ]
                }
            ]
            }).getAsObject();

            expect(box.center).toEqual([3.0, 0.5, -2]);
            expect(box.sizeHalf).toEqual([3.0, 3.5, 3.0]);
        });
    });
});
