/**
* @copyright 2019 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

const Vector3 = require("../../src/math/vector3");

describe('Vector3', () => 
{
    describe('length', () => 
    {
        test('should be zero if empty', () => {
            const vec = new Vector3();
            expect(vec.length()).toEqual(0.0);
        });

        test('simple vector', () => {
            const vec = new Vector3([1.0, 2.0, 3.0]);
            expect(vec.length()).toBeCloseTo(3.74, 2);
        });

        test('negative simple vector', () => {
            const vec = new Vector3([-1.0, -2.0, -3.0]);
            expect(vec.length()).toBeCloseTo(3.74, 2);
        });
    });

    describe('normalize', () => 
    {
        test('zero vector should return zero vector', () => {
            const vec = new Vector3();
            expect(vec.normalize().vec).toEqual([0.0, 0.0, 0.0]);
        });

        test('axis vector', () => {
            const vec = new Vector3([0.0, 2.0, 0.0]);
            expect(vec.normalize().vec).toEqual([0.0, 1.0, 0.0]);
        });

        test('simple vector', () => {
            const vec = new Vector3([1.0, 2.0, 3.0]);
            const norm = vec.normalize().vec;
            expect(norm[0]).toBeCloseTo(0.267, 3);
            expect(norm[1]).toBeCloseTo(0.535, 3);
            expect(norm[2]).toBeCloseTo(0.802, 3);
        });

        test('should not modify original vector', () => {
            const vec = new Vector3([0.0, 2.0, 0.0]);
            vec.normalize();
            expect(vec.vec).toEqual([0.0, 2.0, 0.0]);
        });
    });

    describe('subtract', () => 
    {
        test('simple vector', () => {
            const vec = new Vector3([1.0, 2.0, 3.0]);
            const diff = vec.subtract(new Vector3([2.0, 1.0, 0.5])).vec;

            expect(diff[0]).toEqual(-1.0);
            expect(diff[1]).toEqual(1.0);
            expect(diff[2]).toEqual(2.5);
        });

        test('should not modify original vector', () => {
            const vec = new Vector3([0.0, 2.0, 0.0]);
            vec.subtract(new Vector3([1.0, 2.0, 3.0]));
            expect(vec.vec).toEqual([0.0, 2.0, 0.0]);
        });
    });

    describe('add', () => 
    {
        test('simple vector', () => {
            const vec = new Vector3([1.0, 2.0, 3.0]);
            const diff = vec.add(new Vector3([2.0, 0.0, 0.5])).vec;

            expect(diff[0]).toEqual(3.0);
            expect(diff[1]).toEqual(2.0);
            expect(diff[2]).toEqual(3.5);
        });

        test('should not modify original vector', () => {
            const vec = new Vector3([0.0, 2.0, 0.0]);
            vec.add(new Vector3([1.0, 2.0, 3.0]));
            expect(vec.vec).toEqual([0.0, 2.0, 0.0]);
        });
    });

    describe('cross', () => 
    {
        test('simple vector', () => {
            const vec = new Vector3([1.0, 0.0, 0.0]);
            const cross = vec.cross(new Vector3([0.0, 1.0, 0.0])).vec;

            expect(cross[0]).toEqual(0.0);
            expect(cross[1]).toEqual(0.0);
            expect(cross[2]).toEqual(1.0);
        });

        test('should not modify original vector', () => {
            const vec = new Vector3([0.0, 2.0, 0.0]);
            vec.cross(new Vector3([1.0, 2.0, 3.0]));
            expect(vec.vec).toEqual([0.0, 2.0, 0.0]);
        });
    });

    describe('negate', () => 
    {
        test('simple vector', () => {
            const vec = new Vector3([1.0, -2.5, 0.0]);
            const neg = vec.negate().vec;

            expect(neg[0]).toEqual(-1.0);
            expect(neg[1]).toEqual(2.5);
            expect(neg[2]).toEqual(-0.0);
        });

        test('should not modify original vector', () => {
            const vec = new Vector3([0.0, 2.0, 0.0]);
            vec.negate();
            expect(vec.vec).toEqual([0.0, 2.0, 0.0]);
        });
    });

    describe('scale', () => 
    {
        test('scale', () => {
            const vec = new Vector3([1.0, 2.0, -3.0]);
            const newVec = vec.scale(0.5).vec;

            expect(newVec[0]).toEqual(0.5);
            expect(newVec[1]).toEqual(1.0);
            expect(newVec[2]).toEqual(-1.5);
        });

        test('should not modify original vector', () => {
            const vec = new Vector3([1.0, 2.0, 3.0]);
            vec.scale(1.2345);
            expect(vec.vec).toEqual([1.0, 2.0, 3.0]);
        });
    });

    describe('dot', () => 
    {
        test('simple vector', () => {
            const vecA = new Vector3([-2.0, 1.0, 0.0]);
            const vecB = new Vector3([4.0, 2.0, 2.0]);
            expect(vecA.dot(vecB)).toEqual(-6.0);
        });
    });

    describe('copy', () => 
    {
        test('simple vector', () => {
            const vecA = new Vector3([-2.0, 1.0, 0.0]);
            const vecB = vecA.copy();
            expect(vecA.vec).toEqual(vecB.vec);
        });

        test('should not modify original vector', () => {
            const vecA = new Vector3([-2.0, 1.0, 0.0]);
            const vecB = vecA.copy();
            expect(vecA.vec).toEqual(vecB.vec);
            vecA.vec[0] = 42.0;
            expect(vecA.vec).not.toEqual(vecB.vec);
        });
    });

    describe('min/max', () => 
    {
        test('min', () => {
            const vec = Vector3.min(
                new Vector3([-2.0, 0.0, 42.0]),
                new Vector3([2.0, 1.0, 20.0])
            ).vec;
            expect(vec).toEqual([
                -2.0, 0.0, 20.0
            ]);
        });

        test('max', () => {
            const vec = Vector3.max(
                new Vector3([-2.0, 0.0, 42.0]),
                new Vector3([2.0, 1.0, 20.0])
            ).vec;
            expect(vec).toEqual([
                2.0, 1.0, 42.0
            ]);
        });
    });
});