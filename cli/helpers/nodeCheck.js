const semver = require('semver');

module.exports.checkVersion = function checkVersion(testingError=false) {
  const satisfiedVersion = semver.satisfies(process.version, '>= 11.6.0');
  if (satisfiedVersion & !testingError) {
    return true;
  } else {
    throw new Error('Unmet dependency! - Node.js version 11.6.0 not satisfied\nA version can be acquired here: https://nodejs.org/en/download/current/');
  }
}