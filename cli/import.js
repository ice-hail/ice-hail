#!/usr/bin/env node
/**
* @copyright 2019 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

const packageJson = require('../package.json');
const fs = require('fs-extra');
const path = require("path");
const program = require('commander');
const colors = require('colors');
const nodeCheck = require('./helpers/nodeCheck');
const convertFile = require('../src/converter/convert');

program.version(packageJson.version, '-v, --version')
    .arguments('<modelFile> [outputFile]', 'Imports a 3D-Model and generates a JSON file from it')
    .option('-c, --compound', 'Import as an compound shape (shrine/field)', program.BOOL, false)
    .option('-o, --offset <values>', 'Offset to move the model, e.g. "1.2,3,4.2"')
    .option('-p, --pretty', 'Beautify the created JSON file', program.BOOL, false)
    .action(async (modelFile, outputFile, options) =>  importModel(modelFile, outputFile, options).catch((e) => {
        console.error(e);
        process.exit(1);
    }))
    .parse(process.argv);

if (process.argv.length < 3) {
    program.help();
}

async function importModel(modelFile, outputFile, options)
{
    try {
        nodeCheck.checkVersion();
    } catch (e) {
        console.error(colors.red(e.message));
        return;
    }

    if(options.offset) {
        options.offset =  options.offset.split(",").map(x => parseFloat(x)).slice(0, 3);
        console.log("Using offset: ", options.offset);
    }

    outputFile = outputFile || path.basename(modelFile, '.obj') + ".json";

    console.log("Converting file...");
    const json = await convertFile(modelFile, options);

    console.log("Saving JSON...");
    fs.writeJson(outputFile, json, options.pretty ? {spaces: 2} : {});

    console.log(`File saved to '${outputFile}'`);
};