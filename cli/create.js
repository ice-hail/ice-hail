#!/usr/bin/env node
/**
* @copyright 2019 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

const packageJson = require('../package.json');
const fs = require('fs-extra');
const program = require('commander');
const colors = require('colors');
const nodeCheck = require('./helpers/nodeCheck');
const encodeYaz0 = require('../src/yaz0/encode');
const generateFile = require("../src/generateFile");

program.version(packageJson.version, '-v, --version')
    .option('-y, --yaz0', 'Compress output file with yaz0', program.BOOL, false)
    .arguments('<jsonFile> [outputFile]', 'Creates a binary HK file from a JSON file')
    .action((jsonFile, outputFile, options) => create(jsonFile, outputFile, options).catch((e) => {
        console.log(options);
        console.error(e);
        process.exit(1);
    }))
    .parse(process.argv);

if (process.argv.length < 3) {
    program.help();
}

async function create(jsonFile, outputFile, options)
{
    try {
        nodeCheck.checkVersion();
        console.log(colors.green('Node.js version check - SUCCESS'))
    } catch (e) {
        console.error(colors.red(e.message));
        return;
    }

    console.log(`Loading JSON file '${jsonFile}'...`);
    const collData = await fs.readJSON(jsonFile);        
    
    if (!outputFile) {
        const extPrefix = options.yaz0 ? ".s" : ".";
        const extension = extPrefix + collData.compound ? "hksc" : "hkrb";
        outputFile = jsonFile.split(".").slice(0,-1).join(".") + extension;
    }

    console.log(`Creating HK-File file...`);
    const hkBuffer = generateFile(collData);

    try {
        if (options.yaz0) {
            await encodeYaz0(hkBuffer, outputFile);
        } else {
            await fs.writeFile(outputFile, hkBuffer);
        }
    } catch (e) {
        console.log(e);
    }

    console.log(`File saved to '${outputFile}'`);
}