/**
* @copyright 2019 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

module.exports = {
    "default": 0x00008142,
    "Material_Soil": 1,
    "Material_Stone": 2,
    "Material_Sand": 3,
    "Material_Metal": 4,
    "Material_WireNet": 5,
    "Material_Grass": 6,
    "Material_Wood": 7,
    "Material_Water": 8,
    "Material_Snow": 9,
    "Material_Ice": 10,
    "Material_Lava": 11,
    "Material_Bog": 12,
    "Material_HeavySand": 13,
    "Material_Cloth": 14,
    "Material_Glass": 15,
    "Material_Bone": 16,
    "Material_Rope": 17,
    "Material_CharControl": 18,
    "Material_Ragdoll": 19,
    "Material_Surfing": 20,
    "Material_GuardianFoot": 21,
    "Material_HeavySnow": 22,
    "Material_Unused0": 23,
    "Material_LaunchPad": 24,
    "Material_Conveyer": 25,
    "Material_Rail": 26,
    "Material_Grudge": 27,
    "Material_Meat": 28,
    "Material_Vegetable": 29,
    "Material_Bomb": 30,
    "Material_MagicBall": 31,
    "Material_Barrier": 32,
    "Material_AirWall": 33,
    "Material_Misc": 34,
    "Material_GrudgeSlow": 35,
};