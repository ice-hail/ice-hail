/**
* @copyright 2019 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

const fs = require('fs-extra');
const path = require('path');
const readline = require('readline');
const materialList = require("../materialList");

const materialNamesShort = Object.keys(materialList)
    .map(name => name.replace("Material_", ""));

const parseObj = require('./parser/obj');

module.exports = async (modelFile, options) => 
{
    let modelData = {};

    try{await fs.access(modelFile)} catch(e) {
        throw `File '${modelFile}' does not exist or can't be accessed!`;
    }

    if(modelFile.endsWith(".obj")) {
        let fileStream = readline.createInterface({
            input: fs.createReadStream(modelFile, {encoding: 'utf8'})
        });
        modelData = await parseObj(fileStream, options);
    }

    if(options.compound) {
        return await createCompoundJson(modelData);
    } else {
        throw "Error: import of non-compound shapes is not supported at the moment, sorry!\nUse the '-c' flag for compound (shrine/field) files.";
    }
};

function getMaterialByName(name)
{
    for(let mat of materialNamesShort) {
        if(name.includes('[' + mat + ']')) {
            return "Material_" + mat;    
        }
    }

    return "Material_Stone_DgnLight,Wall_NoClimb";
}

async function createCompoundJson(modelData)
{
    const template = await fs.readJSON(path.join(__dirname, 'templates/compound.json'));

                          // root -> physics ->  system -> rigidBody -> compound
    const compound = template.data.children[0].children[0].children[0].children[0];

    template.compound.actors = modelData.objects.map((model, i) => (
        {
            id: i,
            hashId: i.toString(16).padStart(8, '0'),
            strHash: "00000000"
        }
    ));

    compound.children = modelData.objects.map((model, i) => ({
        type: "hkpBvCompressedMeshShape",
        actorId: i,
        materials: [getMaterialByName(model.name)],
        parts: [
            {
                vertices: model.vertices,
                indices: model.indices,
            }
        ]
    }));
    
    return template;
}
