/**
* @copyright 2019 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*
* The Yaz0 script used here (ported_code.cpp) was ported from:
* Author: Shadsterwolf
* Repo: https://github.com/Shadsterwolf/BotWUnpacker/
* License: LICENSE filr in this directory
*/

const fs = require('fs');
const wasmModule = require("./yaz0-lib.js");

function getYaz0Instance()
{
    return new Promise((resolve, reject) => 
    {
        try{
            instance = new wasmModule({
                onRuntimeInitialized() {
                    delete this["then"]; // well fuck you emscripten!
                    resolve(this);
                }
            });
            
        }catch(e) {
            console.error(e);
            reject(undefined);
        }
    });
}

module.exports = async function encodeYaz0(buffer, filePath)
{
    const instance = await getYaz0Instance();

    const maxYaz0Size = instance.getEncodeSize(buffer.length);

    var hrstart = process.hrtime();

    var ptrInput = instance._malloc(buffer.length);
    const inputBuffer = new Uint8Array(instance.HEAPU8.buffer, ptrInput, buffer.length);
    buffer.copy(inputBuffer);
    
    var ptrOutput = instance._malloc(maxYaz0Size);

    const outputLength = instance.encode(ptrInput, buffer.length, ptrOutput);
 
    const resBuff = new Uint8Array(instance.HEAPU8.buffer, ptrOutput, outputLength);
    fs.writeFileSync(filePath, resBuff);

    instance._free(ptrInput);
    instance._free(ptrOutput);

    var hrend = process.hrtime(hrstart);
    console.info('Yaz0 encoding time: %ds %dms', hrend[0], hrend[1] / 1000000);
}