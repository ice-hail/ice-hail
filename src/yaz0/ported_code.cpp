/**
 * Original Author: Shadsterwolf
 * https://github.com/Shadsterwolf/BotWUnpacker/
 * 
 * Ported to C++/WASM by HailToDodongo
 * 
 * @TODO put whole project into a seperate repo
 * This is only the ported code, the project around it will be a seperate repo soon
 */

#include <emscripten/bind.h>
#include "../include/main_header.h"

using namespace emscripten;

int getEncodeSize(u32 uncompressedSize)
{
    return 16 + uncompressedSize + (uncompressedSize/2);
}

u32 encode(u32 dataPtr, u32 uncompressedSize, u32 encodedPtr)
{
    u8* inFile = (u8*)dataPtr;
    u8* encodedData = (u8*)encodedPtr;

    u32 dataOffset = (inFile[0x0C] << 24) | (inFile[0x0D] << 16) | (inFile[0x0E] << 8) | inFile[0x0F];

    //Encode Logic
    u32 sourcePos = 0; //start of data after header
    u32 groupPos;
    u32 copyPos;

    u8 groupData[24]; //24 bytes at most in special cases, 16 if all normal pairs, 8 if all straight copy...
    u8 groupHeader;
    std::string groupHeaderFlag; //To build the binary flags to clearly know what is being flagged, then convert to a byte.
    u32 numBytes = 0;
    u32 rleNumBytes;
    u32 copyNumBytes;
    u32 predictNumBytes;
    u32 predictCopyPos;
    bool predictHit = false;
    u32 bufferPos;
    u32 seekPos;
    u16 dataCalc;
    u32 fixedOffset;
    
    if (dataOffset == 0x2000)
        fixedOffset = dataOffset;
    else
        fixedOffset = 0x00;

    // set header
    encodedData[0] = 'Y';
    encodedData[1] = 'a';
    encodedData[2] = 'z';
    encodedData[3] = '0';

    encodedData[4] = (uncompressedSize >> 24) & 0xFF;
    encodedData[5] = (uncompressedSize >> 16) & 0xFF;
    encodedData[6] = (uncompressedSize >>  8) & 0xFF;
    encodedData[7] = (uncompressedSize >>  0) & 0xFF;

    encodedData[ 8] = (fixedOffset >> 24) & 0xFF;
    encodedData[ 9] = (fixedOffset >> 16) & 0xFF;
    encodedData[10] = (fixedOffset >>  8) & 0xFF;
    encodedData[11] = (fixedOffset >>  0) & 0xFF;

    encodedData[12] = 0;
    encodedData[13] = 0;
    encodedData[14] = 0;
    encodedData[15] = 0;

    u32 writePos = 16; // skip header

    while (sourcePos < uncompressedSize)
    {
        memset(groupData, 0, sizeof groupData);
        groupHeaderFlag = "";
        groupPos = 0; //first byte
        while (groupHeaderFlag.length() < 8) //ensure number of Header Flags is less than 8, as group can be between 8-16 bytes
        {
            rleNumBytes = 3; copyNumBytes = 3; predictNumBytes = 3; // reset
            copyPos = 0; predictCopyPos = 0;
            bool match = false;

            if (sourcePos != 0 && (sourcePos + 3) < uncompressedSize)
            {
                //RLE check
                if ((inFile[sourcePos] == inFile[sourcePos - 1]) && (inFile[sourcePos + 1] == inFile[sourcePos - 1]) && (inFile[sourcePos + 2] == inFile[sourcePos - 1]) && predictHit != true) //Match found for RLE/overlap
                {
                    match = true;
                    bufferPos = sourcePos + 3; //buffer source ahead
                    copyPos = sourcePos - 1;
                    if (bufferPos < uncompressedSize)
                    {
                        while (bufferPos < uncompressedSize && inFile[bufferPos] == inFile[sourcePos - 1] && rleNumBytes < (0xFF + 0xF + 3)) //while there is more data matching from that one byte... (don't ask about the math plz, even I am confused)
                        {
                            rleNumBytes++;
                            bufferPos++;
                        }
                    }
                }   
                //Copy check
                for (u32 backPos = sourcePos - 1; backPos > 0 && (sourcePos - backPos) < 0xFFF; backPos--) //go backwards into the inFile data from current position and search for a matching pattern
                {
                    if (inFile[sourcePos] == inFile[backPos] && inFile[sourcePos + 1] == inFile[backPos + 1] && inFile[sourcePos + 2] == inFile[backPos + 2]) //Match found for copy
                    {
                        match = true;
                        seekPos = backPos + 3; //search ahead
                        bufferPos = sourcePos + 3; //buffer source ahead

                        if (copyPos == 0) //if there is no copy position recorded...
                            copyPos = (u32)backPos;

                        u32 instanceNumBytes = 4;
                        if (bufferPos < uncompressedSize && seekPos < uncompressedSize)
                        {
                            while (bufferPos < uncompressedSize && seekPos < uncompressedSize && inFile[bufferPos] == inFile[seekPos] && copyNumBytes < (0xFF + 0xF + 3)) //while there is more data matched, and the seek position is less than the source position...
                            {
                                if (copyPos != backPos) //if new potential position is found
                                {
                                    if (copyNumBytes < instanceNumBytes) //if current numBytes is less than new instance, take new position and increment
                                    {
                                        copyPos = (u32)backPos;
                                        copyNumBytes++;
                                    }
                                }
                                else
                                    copyNumBytes++;
                                instanceNumBytes++;
                                seekPos++;
                                bufferPos++;
                            }
                        }
                    }
                    if (inFile[sourcePos + 1] == inFile[backPos] && inFile[sourcePos + 2] == inFile[backPos + 1] && inFile[sourcePos + 3] == inFile[backPos + 2]) //Predict
                    {
                        seekPos = backPos + 3; //search ahead
                        bufferPos = sourcePos + 4; //buffer source ahead, predicted

                        if (predictCopyPos == 0) //if there is no copy position recorded...
                            predictCopyPos = (u32)backPos;

                        u32 instanceNumBytes = 4;
                        if (bufferPos < uncompressedSize && seekPos < uncompressedSize)
                        {
                            while (bufferPos < uncompressedSize && seekPos < uncompressedSize && inFile[bufferPos] == inFile[seekPos] && predictNumBytes < (0xFF + 0xF + 3))
                            {
                                if (predictCopyPos != backPos) //if new potential position is found
                                {
                                    if (predictNumBytes < instanceNumBytes) //if current numBytes is less than new instance, take new position and increment numBytes
                                    {
                                        predictCopyPos = (u32)backPos;
                                        predictNumBytes++;
                                    }
                                }
                                else
                                    predictNumBytes++;
                                instanceNumBytes++;
                                seekPos++;
                                bufferPos++;
                            }
                        }
                    }

                    //if (sourcePos >= 0x3DA9 && (sourcePos - backPos) > 3835) //debug encode
                        //System.Windows.Forms.MessageBox.Show("SourcePos: 0x" + sourcePos.ToString("X") + "\n" + "searchPos: " + "0x" + backPos.ToString("X") + "\n" + "copyPos: 0x" + copyPos.ToString("X") + "\n" + "predictCopyPos: 0x" + predictCopyPos.ToString("X") + "\n" + "dist: " + (sourcePos - backPos) + "\n" + "copyNumBytes: " + copyNumBytes + "\n" + "predictNumBytes: " + predictNumBytes);
                }
                predictHit = false; //reset prediction
                if (rleNumBytes >= copyNumBytes) //use RLE number of bytes unless copyNumBytes found a better match
                    numBytes = rleNumBytes;
                else
                    numBytes = copyNumBytes;
                if (predictNumBytes > numBytes)
                {
                    match = false; //flag the next byte as straight copy because the next one will solve one copy instead of two. (End up using 3 bytes instead of 4)
                    predictHit = true;
                }
            }
            if (match) //Flag for RLE/copy
            {
                if (numBytes > 18)
                    dataCalc = (u16)(((0x0) << 12) | (sourcePos - copyPos) - 1); //Mark the 4-bits all 0 to reference the 3rd byte to copy
                else
                    dataCalc = (u16)(((numBytes - 2) << 12) | (sourcePos - copyPos) - 1); //Calculate the pair
                groupData[groupPos] = (u8)(dataCalc >> 8); //b1
                groupData[groupPos + 1] = (u8)(dataCalc & 0xFF); //b2
                groupPos += 2;
                sourcePos += numBytes; //add by how many copies
                groupHeaderFlag += "0";
                if (numBytes >= 18) //if numBytes is greater than 18, but it will be used to accomodate the large number of bytes to copy, do not flag nor increment as it's part of the pair
                {
                    groupData[groupPos] = (u8)(numBytes - 18);
                    groupPos++;
                }
            }
            else if (sourcePos + 1 > uncompressedSize) //End of encryption
            {
                groupHeaderFlag += "0";
                sourcePos++;
            }
            else //Flag for Straight copy
            {
                groupData[groupPos] = inFile[sourcePos];
                groupPos++;
                sourcePos++;
                groupHeaderFlag += "1";
            }
            
        }//end while


        groupHeader = 0;
        for(int i=0; i<8; ++i)
        {
            groupHeader |= (groupHeaderFlag[7-i] == '1' ? 1 : 0) << i;
        }

        encodedData[writePos] = groupHeader;
        writePos++;
        for(int k = 0; k < groupPos; k++)
        {
            encodedData[writePos] = groupData[k];
            writePos++;
        }
        
    }//end while
    
    return writePos;
}

EMSCRIPTEN_BINDINGS(my_module) {
  function("encode", &encode, allow_raw_pointers());
  function("getEncodeSize", &getEncodeSize);
}