## v3.5.0
- OBJ importer noe supports all materials (see readme)

## v3.4.1
- support for more materials (see readme)

## v3.4.0
- support for materials: default, Stone, Grass (climbable)
- re-spawning on geometry now works

## v2.0.0
- new file format version 4 -> same structure as the native files
- compressed mesh support
- compound file (aka shrine/field) support
- model importer
- new file parser
- bugfix in class header, offset is always now correct
- completely refactored code
- better test coverage

## v1.3.0
- new shape type: sphere

## v1.2.0
- added climbable flag to entries